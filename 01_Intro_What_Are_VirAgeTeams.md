# Virtual Agile Teams - Introduction



## What is it for me from reading this Article?

If you are satisfied with your Status Quo, thie material is not for you.  We would like to save your time,  Stop here.  We wish you a Great Life in whatever you do!

Even if you enjoy your current job, your professional situation, your opportunities and paths that you currently have, you may find some ideas and concepts interesting to consider, if not in any other but just as "a mental WHAT-IF exercise" way.

If you are dissatisfied with your current job or profesional situation, if you are searching for new ways to make your living, to find satisfaction in what you do, seeking balance in your personal and professional lives, want to be the Master of your own destiny, and you are open to new ideas, we hope this article gives your some perspectives and possible opportunities to consider.  

And finally, if you are actively seeking a partnership and collaboration with other professionals in Technology, Digital Economy, and new forms of Wealth creation through productive and rewarding work (not passive income generation), we hope that you will join our effort in building Virtual Agile Teams Collective.   If this material generates more questions for you than we can answer, we truely hope that you will be the one to FIND ANSWERS TO YOUR QUESTIONS, as we work together with you for our mutual benefit and sustainability of our econmic well-being.



## Intended Audience:

You are a professional in any area of Knowledge that benefits from the use of computing technology, or directly related to creation of any computing products and services ("As Code" model, configuration, composition or any other complex human decisions that are non-standard and specific to each concrete problem domain in digital industry).  In other words, you have to make decisions in your work creating new digital products or services, even if you are employing AI as a tool.  You can be on any professional level (novice or expert) and your age and social position do not matter.  The only personal trait that is expected to help you achieve maximum benefits from participating in the proposed model is your desire to exit "wage slavery", be in control of your career and professional growth, ability to work in a team and alternate the opportunites to lead and to follow in achieving common goals to which you consent in collaboration with your team mates.  Commitment is a big factor in achieving success, but a Failure, in whatever you do, is only certain IF YOU NEVER TRY. 



## Objective: 

Virtual Agile Teams - VirAge Teams or VAT - is envisioned as a platform for decentralized autonomous teams of collaborators and partners with equal decision-making rights through democratic voting, consensus and commitment to set goals for gainful work in creating value for customers, clients and the VirAge Teams organization itself.   VirAge Teams is an alternative to the corporate employment model of Employer-Employee relationship.  Each partner in VirAge Teams is a business owner of the business entity, which is a team in which he/she participates in collaboration with other partners.  Partners are in full and complete control of their business, products, services, clients and customers, and as such, partners assume all risks and benefits of owning and running the business.  Partners do not receive a salary or wage for their work, but they contribute to the value created by their Team, and for this they particpate in the patronage distribution (share of the value).  Partners do not pay employment taxes, and neither does VirAge Teams, as an organization, withhold any employment or social security taxes.  Partners are compensated through annual or more frequent Patronage Distribution of Surplus remaining after payment of all costs and expenses of doing business and operation.  Partners are responsible for their own taxes from the received Patronage Distributions, as owners of business, according to the local laws of their primary residence.  

Virtual Agile Teams is structured as a Limited Liability Company for simplicy of operation in the State of California, abides by the US Federal, California State and the Los Angeles Country tax regulation and operates as a Collective of federated Collectives or Co-Operatives (Co-Ops).  United on the single VAT Platform, various Collectives can be formed to achieve specific goals, defined as Value Streams, to enable sustainability of business, continuous operations, continuous work and income opportunity for its members.  

VirAge Teams is a for-profit organization, but we do not use the term "Profit", as it is a corporate term for retaining the added value of products and services sold for the corporate owners.  VirAge Teams uses the term "Surplus" that is distributed among partners at the established rate of compensating contributions for the value created.   



## Definitions:

**Digital.**  As a technology, Digital is a collection of tools, services, products, as well as the outcomes achieved from applying such tools, services and products that are accessible and useful for humans only through computing devices.  As an economic factor, Digital is the process of human interactions, wealth creation and distribution, that is carried out through the technology defined as Digital.  Naturally, Digital has close relationship to the media and delivery technology (web, mobile, IoT devices, sensors, etc.).  Cyber-Physical products also can be considered part of Digital, however manufacturing phisical devices (or any phisical objects) is outside the scope of Digital for the purpose of VirAge Teams (for now).

**Knowledge Workers (KWs).**  These are the humans involved in creation of value streams through products and services developed with Digital (as a Technology) to be delivered to end users or clients of VirAge Teams who in turn deliver it to their customers.  KWs are characterized by their Decision-Making ability in creating new value.  Their main attributes are:

* Continous learning and improving skills and competencies
* Collective work translating individual competencies into capabilities to create complete products (at least, minimal viable products - MVPs)
* Focus on delivery of valuable outcomes (vs. generating outputs, as "noise", "heat" or any wasteful by-product of any activity), hence KWs are natively prone to work in the "Agile" mode
* KWs own and improve the entire delivery pipeline from Ideation stage to Deployment.  
* KWs are the owners of their Intellectual Property, they don't sell their time for money, and in case IP must be transfered to the clients, who commissioned their work, KWs sell the result of their work in a business transaction, not through an employment contract.

**Platform.**  VirAge Teams builds and operates a platform on which Partner-Members can collaborate, account for their contributions to their teams, record their achievements, competencies and credentials; record, monitor and verify any revenues, expenses, distributions and other money transactions in and out of the VirAge Teams accounting systems; build the system of Trust, Commitment and Consensus for any activity, either income generating or to build and improve the platform (in corporate terms called as the "Cost Center Operations").  The Platform is distributed and decentralized, as each Partner maintains and controls a Node in the system.   Why the Platform is not centralized?  It is because the Platform must provide Fairness and Trust between all Partners not tied through direct Command-Control or Master-Subordinate or Employer-Employee relationships.  As Platform matures, it will be transitioned to more AI-driven operations that eliminate the opportunity for conflict prone to human controlled employment structures.  

**Collective or Co-Operative (Co-Op).**   It is not a new organization model.  The ownership and equity in the Co-Op company belongs to Members-Partners-Workers.  The US and many State and local legislations support the Co-Op movement, however so far, this form of organization was primarily applied in agricultural/farm business or low-skilled, low-revenue generation enterprises.   Some Collectives started to appear in Digital, primarily focused on specific functions, like Art and Graphics studios, Web Development, and so on.  Usually, Co-Ops have a Charter of "one worker - one vote" and are managed by the elected board of managers or directors.   

**Value Streams.**  If you are not familiar with the idea of Value Streams, read the following article from SAFe on its [10th Principle of Organizing around the Value](https://scaledagileframework.com/organize-around-value/).  Value Streams is the core organizational principle for VirAge Teams.  All Partners work towards some value, either for the organizaiton itself or to its clients, in the form of products and services.  Unlike a traditional consulting company, or independent contractor's work, VirAge Teams' Value Streams create the foundation for acquiring and sharing Knowledge between Partners in order to build a sustainable work and business operation.  

**Dynamic Equity.**   The book and concept of ["Slicing Pie"](https://slicingpie.com) have popularized the idea of the Dynamic Equity.  VirAge Teams extends this idea beyond the inital phases of start-ups because VirAge Teams is a startup that continues on and on.  It's worth and value is established by the contributions of all its members.  The concept is very simple: as long as VirAge Teams Value Streams continue to exist and generate revenue, all Member-Partners involved in the Value Stream creation and maintenance are entitled to receive benefits.

**Wealth.**  As sometimes referred to as: "Digital Wealth", this is the factor of how much the results of someone work (labor) have translated into durable benefits for the person.  Salary and wages are not the Wealth in this sense.  They are a mere compensation for one's time creating a value.  In the industrial age, the Wealth stays with the "Capitalist" who invested money, acquired land, machines and tools, hired workers and made them perform the work for which they got their time-based or skills-based compensation.  The results of labor stayed with the proprietor.  In Digital, the Wealth is the result of the Knowledge Workers' work that can be created WITHOU investment in land, machines, tools, etc.  A Knowledge Worker is an educated individual, whether with a formal college/university diploma or self-taught in the field of his/her calling and interest.  Although a formal education is traditionally more valued as someone's proof of effort into acquiring skills, continuous self-education and learing may eventually be even more valuable as it provides specific and very pointed knowledge for resolving specific problems.  In Digital, a Knowledge Worker does not need to have a special Employer provided work place, like a factory or an office that require substantial investment.  Ideation, problem resolution, system architecture, design, code or content development, with subsequent deployment to scaled tiers of delivery (for validation, testing, pre-production and finally, the full-scaled production mode to all intended users), can be done at low costs, even covered by the Knowledge Workers themselves.  What used to be a significan Capital investment into land, machinery and tools, has turned into Operating Costs of utility services, that now, in addition to Compute, Networking and Storage in the Cloud also more and more inclue Machine Learning, Language Models and other AI plug-in services.  Given all of the above, there is no reason for Knowledge Workers to give up the Wealth generated from the products created for the benefit of someone else.  Negotiaton about the Wealth retaine by KWs and the Wealth shared with other players should be done on the Busienss-to-Business level, not by the Employment contract. 



## Problem: 

The Industrial Age work process organization is outdated for the Digital Age.  In fact, it is completely unacceptable to assume that the Knowledge Workers creating digital assets and wealth through digital technology and applying daily decision-making process to assymilar problems will operate as assembly line workers.  

Knowledge Work is part art, part science.  It does not fit the standards of the 19th century model on any level.  KWs work in teams and combine their individual competencies into collective capabilities to deliver products.   The modern work process organization is Agile but it has one major flaw: Agile method is created for the benefit of corporate employer to increase teams' productivity with no benefit for workers and team members.  This can be fixed with a simple transition of the team from the employee team into a team of partners organized into a Collective.  Now, instead of selling their time, they sell the capability to create a digital product or service as the result of their labor.  Of course, this transition is not so simple, but unless teams (especially those that are well adjusted and fit to work together) start moving out of corporate domain into independent teams, their situation will not change.  This is a common practice, as outsourcing model gain popularity in the 1990s-2000s.  Many of us who worked in those days remember when, with a simple contract signature of the corporate bosses, entire teams and departments were moving from one employer (the client) to another employer (the outsourcing consulting company).  After years of service to the employer, people were losing their benefits and the status of working for the brand that many had grown to associate with their own success and achievement to some not-so-know or not-so-prestigious brands of consulting (in many cases foreign offshore) companies.  

Every employee working for a corporate entity must understand, there is no corporate loyalty and fairness in any employment contract.  Unless a person works for themselves, they will always be running the risk of being downsized, layed off, denied promotion and raise, including various bureaucratic obstacles in changing career paths, receiving education outside the main line of their work activity and many other factors.  "Wage slavery" is a predominate culture phenomenon in the Western white collar industries.  

Nothing examplifies the problem of "wage slavery" better than the requirement for Knowledge Workers to work on site, despite the modern means of communications, remote computing and virtual environments for any activity in Digital.  Unless you are employed in a highly secure environment where a physical "Air Gap" is required between the computing systems connected to the outside and the internal highly secure devices, there is no real need for workers to be co-located with the devices on which the computation process takes place.  For this reason, many KW welcomed the opportunity to use remote work arrangement and moved out of big employment hubs to suburbs or remote areas where they could do their jobs during the 2020-2021 Pandemic.  




## Context:

Digital Wealth and Knowledge can be created by the collective effort of cooperating teams.  Teams have power to innovate, seek solutions and experiment.  Teams have ownership of their ideas and products.  Teams finance their products through their Knowledge Capital (individual competencies amplified through collective collaboration, which creates capability to deliver and deploy a product).

The nature of work is Decentralized and Distributed.  The collaborating talents do not have to be co-located or work in the same physical space.  The trend to decentralization (either driven culturally or because of the major shakeups in the economy) becomes a major disruption trigger for the traditional corporate systems of Command and Control:  HR databases, Accounts Receivable/Payable, Revenue Management, Supply Chains, Enterprise Planning, etc.  Monolithic corporate IT systems proved cumbersome to maintain and operate, once corporations, under the pressures of the breaking Globalization model, disrupted supply chains, the existential survivial need to seek new markets and transition to new products and services.   As the advocates for the SAFe Agile Framework model indicate, the corporate world must adapt the new "dual operating system model" in which the traditional structures co-exist with the new agile ad-hoc and fluid organizations based on developing and supporting the new Value Streams. 

Value Streams is the core construct in VirAge Teams and all activities of Partners must align to them.  When Value is created, it can be translated into Wealth (though revenues received from external clients who pay for the value) or through the equity in the internal product created to support VirAge Teams operations.   Part of costs and expenses that VirAge Teams will cover from the revenues from the external clients will go to compensate the contribution to those Partners who only worked on the internal products (internal Value).

When the VirAge Teams model becomes stable, sustainable and profitable, more Knowledge Workers start switching from their corporate jobs or independent contractor gigs to work in long-lasting teams in the Co-Operative.  The model is designed to provide all needed support and foundation for creating new Collectives and new Teams.  It is expected that VirAge Teams will be growing its Capital Account, and that in turn provides necessary resources to expanding and facilitating on-boarding.   Collectively, Members will be able to decide which area of their activity requires most investment and development.  

Unlike investment in stocks, investment in your teams' work provides more decision-making control to individuals who are vested in maximizing the results and subsequent benefits for all.  


## Outputs and Outcomes:

Any economic activity can be described in terms of Outputs and Outcomes. 

Outputs is a routine, sometimes repeated, activity that consumes some energy and creates some results that may or may not be useful for those whom the work was intended to create value.  Outputs can be viewed as transformation of one form or energy into another form (maybe in a form or an object, result, information or anything else) that is different from the initial energy but not completely useful.  Or Outputs can be viewed as costs incurred without utility created,

On the other hand, Outcomes are the desired results from any economic activity and thus drectly related to the value created.  Outcomes are the goals achieved.  If you are playing darts and your goal is to hit the Bullseye, all the darts that land anywhere but the Bullseye are the "Outputs" (you spent your energy throwing them, but you haven't achieved your goal), and the one dart that hits the target right is the "Outcome" - the goal is achieved.  You can say that all the Outputs (the missed darts) helped to create the Output (you were training and getting better and better with every throw).  You are correct.   So Outputs contribute to the final Outcome, but it's impotant to recognized that the Outcome is not achieved no matter how many Outputs you created.  Unless the set objective is not achieved, you don't have the Outcome, no matter how many Outputs you have recorded.

VirAge Teams are focused and dedicated to the Outcomes, and not the Outputs.  All activities of VirAge Teams Members are aligned with the goals and objectives that the Co-Operative collectively establishes for achievement.  Only achieved goals (Outcomes) create value that can be shared by all participating Members. 



## Team Structures:

VirAge Teams Collective makes a big emphasis on the team organization (it's in the name, Silly!).  The Collective will not be different from any other consulting organization, if it just allows to onboard any number of associates and then start moving them around to jobs or projects requested by clients.   VirAge Teams stipulates that the teams are formed first, teams work together, adjust, adapt to the working styles of each other before becoming fully productive to generate revenue.  But of course, everything starts with an individual Member. Contributions of each Member are accounted for on all levels and for all Values Streams that he/she contributes to. 

As you read through the following description of different levels on which a Member makes a contriibution to the final outcome (the product or service), the value of this contribuiton increases on each level.  In other words, as a Member transitions from providing a personal Competency in some activity to enabling a collective Capability in creating a product that can be deployed and operated, the Member's share of value in the collective pot of total value created has increased tenfold.  

* Sole Individual - Only competency in one or a few fields of knowledge.  Lack of Capability (to deliver a product).
* "Troika" - "Trio" - "Trinity" - "Tres Amigos" - A cohesive team of 3 Primary Individuals with 3 core competencies of various or complimentary nature fields that augment and support each others' competency.  Besides the Primary Members of the "Troika", auxiliary or backup members can contribute to Troika's efforts. Troika is defined by increased competency and stability of the output, which is a perfect example when the whole (the Troika) is greater than the sum of its parts (individual competencies of each of the Tres Amigos). 
* TEAM - Also, *VirAge Team* - Equivalent to *Agile Team* in the SAFe Framework.  This formation consists of 3 or 4 Troika's and exemplifies transition from a Competency to a Capability.  TEAM is the core organizational construct of VirAge Teams necessary to generate revenue from external clients or build value in internal products. 
* ART - *Agile Release Train* as per the SAFe Framework.  A number of VirAge Teams (5-6) roll up to an ART on large products or engagements (following the SAFe model).  VirAge ARTs represent the highest Capability possible in order to create and sustain value creation for the benefit of all VirAge Teams Members. For external clients, engagement with VirAge ARTs provides the highest degree of trust and commitment in building a quality product. 

VirAge Teams' core construct is the Troika.   Any problem can be resolved by three Members.  Any engagement can be managed by three Members.  Any other teams can be managed by three Members.   If you visualize VirAge Teams not as a Top-to-Bottom pyramid (alike a Corporate structue), but as a mesh of nodes on a network, each node is a Troika of three Members (with possible auxiliary and support Members in the Troika).  Then this three-member node can act as a branch node for a whole bunch of other "subordinate" nodes (only temporarily for resolving a specific problem), or be itself included into another network of nodes working on some project.   

Larger constructs, like a TEAM, be be used the same way, but due to their size, they become less flexible and more fragile.  Whereas TEAMs can keep structure and coherence for a few projects, Troika should be able to exhibit the most long-lasting characteristics and durability.  




## Team Principles:

Principles are more important than Rules, Architecture, Structures.   Principles are foundational.  They serve to create Rules, Architecture, and Structures.  If Rules become outdated, if Architectures become too rigit for the new reality, if Structures cannot support the ever changing business conditions, Principles should remain the same, because they stem from the values of Members who decided to work collectively to improve their lives.  Deviation from Principles may indicate the end of the Virtual Agile Teams as it was intended. 

* **Trust** - Trust between decentralized and distributed members and teams is based on Verifiable Credentials for individuals and organizations. There is no anonymity among the Members, and all their achievements and credentials are transparent to other Members.  This also applies to contributions and compensation of Members.

* **Product-based** - All collaboration of teams and individuals is based on their commitment to create and release a specific Product or Service that is determined (economically justified) by the Co-Operative an an approved Value Stream.  

* **Fairness** - All participants in the Value Stream who contribute to its creation or operation have the equal right to participate in the benefits that are derived from the revenues that such Value Stream generates.  *Fairness* does not mean *Equality*, and each participant receives the benefits according to thier contribution and their skills and experience level.   

* **Transparency** - All inner-workings of the VirAge Teams organizations are recorded and stored in immutable DLTs with access to all members.  Verifiable Credentials and Zero-Knowledge Trust are important mechanisms applied to ensure confidentiality and security of all internals (and where necessary, external) operations of VirAge Teams.

* **Economic Motivation** - VirAge Teams Model is created to overcome the archaic deficiency of the Wage Employment model (selling personal time for wages) and to promote innovation, entrepreneurship and ownership of results of one's labor.  

* **Dynamic Equity** - Individual contributions to Value Stream are constantly changing, depending on the effort, direct cash or assets contribution and other factors.  Equity is a dynamic element of the created Value.  

* **Ownership of Tasks and Outputs** - Individuals (VirAge Teams Members) have ownership of Tasks and Outputs that must be done in any Value Stream process.  

* **Ownership of Outcome** - It's the VirAge Team's responsibility to own the Outcome, as a final Product or Service, created as the result of the Value Stream execution.  The created Value must translate into Wealth for the individual members.  

* **Verifiable Contribution** - Contribution of every VirAge Team Member should be verifiable and accounted for in distribution of benefits for the duration of the Value Stream.  In other words, any Product or Service generating a revenue, provides for the benefits of all members who worked on the product.   



## Modus Operandi:

Virtual Agile (VirAge) Teams Collective Ltd. is registered as a California LLC.   Perspective members can find out all about [Virtual Agile Teams Ltd.](https://github.com/VirAge-Founder/virage_teams-org) Operating Agreement, Principles and Vision Statement published on GitHub.  

VirAge Teams is a goal-focused, record-based decentralized Collective.  In order to adhere to our principles (e.g. Trust, Transparency, etc.), all decisions, goals and actions to achieve them are recorded.   Operational and working decisions are recorded in GitLab.  In the future, important consensus decisions will be stored in Hedera Hashgraph Distributed Ledger.  

GitLab Public Group for [Virtual Agile Teams](https://gitlab.com/virtual-agile-teams-public) contains all top level programs currently set to bootstrap Virtual Agile Teams operations and member onboarding.  

Currently, Virtual Agile Teams is in the "Ideation" Phase.  This phase is defined as:

* Validation of ideas, concepts, assumptions
* Limited informaiton distribution to a small group of potential Founding Members
* Formation of initial teams of Founders to lead specific VirAge Teams bootstrapping activities
* Content preparation for VirAge Teams promotion of the value proposition and creating awareness among the interested public
* System architecture and infrastructure planning to enable platform development
* Software development of the key features of the platform


## Contact Information and Links

Submit general inquiries to:  virageteams@gmail.com

You can also request joining our Telegram Channel.  The link will be sent to you in reply.

LinkedIn Members can follow our Collective at [Virtual Agile Teams](https://linkedin.com/company/virage-teams) profile. 

Our media content is published at our [YouTube channel](https://www.youtube.com/@VATCo-Op).  

More content and applications will be accessible through [VirAge Teams](https://www.virageteams.com/) website and mobile apps.
