# VAT Articles Repo



## Getting started

This project contains articles and materials that explain and introduce the concept of Virtual Agile Teams.  
This is the recommended place to start familiarizing yourself with the current and the future state of Virtual Agile Teams.  


## Contributing
This project is created by the Founder of the Virtual Agile Teams Ltd. Collective.  The initial content will be provided by the Founder(s).  As VAT develops, a taxonomy of topics for VAT articles will be developed, and all VAT Members will be eligible to contribute to any topic of their interest or subject matter that requires public review and release.  

## Add your files 
- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/virtual-agile-teams-public/strategictheme_marketing-and-communications/portfolio_external_communications/virageteam-articles/vat-articles-repo.git
git branch -M main
git push -uf origin main
```


## Authors and acknowledgment
The Founder is responsible for the initial content in this project.

## License
The materials released in this project are governed by: GNU AFFERO GENERAL PUBLIC LICENSE, Version 3, 19 November 2007.

## Project status
This project is in active development status.
